package  freelancing.newstask.ui.common;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import java.util.Locale;

import freelancing.newstask.R;
import freelancing.newstask.ui.ContextWrapper;
import freelancing.newstask.ui.customviews.ProgressDialogFragment;
import utils.ConnectionUtils;
import utils.LanguageChange;


/**
 * Created by mohamed on 06/04/16.
 */
public class BaseActivity extends AppCompatActivity {

    ProgressDialogFragment mobjProgrss;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }


    public void showProgressDailog(String strProgressData)
    {
        if(mobjProgrss ==null)
        {
            mobjProgrss = new ProgressDialogFragment();
        }

        mobjProgrss.setMessage(strProgressData);

        if (!mobjProgrss.isVisible())
        {
            mobjProgrss.show(getSupportFragmentManager(), strProgressData);

        }
    }

    public void dismissProgressDailog()
    {
        if (mobjProgrss!=null)
        {
            mobjProgrss.dismissAllowingStateLoss();
        }
    }

    public void DisplayMessage(String strMessage, final boolean blnFinishActivity)
    {
        if(!this.isFinishing()){

            final Dialog objDialog = new Dialog(this);

            objDialog.setContentView(R.layout.message_dialog);

            TextView txtMessage = (TextView) objDialog.findViewById(R.id.txtMessage);
            txtMessage.setText(strMessage);

            objDialog.setTitle( Html.fromHtml("<font color='#ad181c'>EVG</font>"));

            // Title divider
            final int titleDividerId =getResources().getIdentifier("titleDivider", "id", "android");
            final View titleDivider = objDialog.findViewById(titleDividerId);

            if (titleDivider != null) {
                titleDivider.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            }




            Button btnOk = (Button) objDialog.findViewById(R.id.btnOk);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("=============================test click");
                    objDialog.dismiss();
                    if(blnFinishActivity)
                    {
                        BaseActivity.this.finish();
                    }
                }
            });

            objDialog.show();

        }
    }


    public boolean checkInternetConnection(){

         return ConnectionUtils.hasInternet(getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        String language= LanguageChange.getLanguage(newBase);

        Context context = ContextWrapper.wrap(newBase, new Locale(language));
        super.attachBaseContext(context);


        System.out.println("+++++++++++++++++++++++++++++attachBaseContext++++++++++++++++++++++++++++++ "+ Locale.getDefault());

    }


}
