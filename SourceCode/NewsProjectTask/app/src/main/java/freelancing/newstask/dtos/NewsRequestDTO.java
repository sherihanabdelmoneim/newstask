package freelancing.newstask.dtos;

public class NewsRequestDTO {

    private String searhText;
    private  int pageNumber;
    private  String language;
    private String  from;

    public NewsRequestDTO(String searhText,String language,String from,int pageNumber)
    {
        this.setSearhText(searhText);
        this.pageNumber = pageNumber;
        this.language = language;
        this.setFrom(from);
        this.pageNumber = pageNumber;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }


    public String getSearhText() {
        return searhText;
    }

    public void setSearhText(String searhText) {
        this.searhText = searhText;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
