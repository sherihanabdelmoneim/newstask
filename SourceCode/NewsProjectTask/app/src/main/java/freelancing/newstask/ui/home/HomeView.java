package freelancing.newstask.ui.home;


import freelancing.newstask.dtos.NewsResponseDTO;
import freelancing.newstask.ui.IView;

/**
 * Created by Sherihan
 */
public interface HomeView extends IView {

     void onSuccessGetNews(NewsResponseDTO newsResponseDTO);
     void onSuccessGetHeadLines(NewsResponseDTO newsResponseDTO);

}
