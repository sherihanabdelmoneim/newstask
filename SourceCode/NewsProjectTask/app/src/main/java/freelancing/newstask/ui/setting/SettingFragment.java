package freelancing.newstask.ui.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import freelancing.newstask.R;
import freelancing.newstask.dtos.Language;
import freelancing.newstask.ui.common.BaseFragment;
import me.anwarshahriar.calligrapher.Calligrapher;
import utils.LanguageChange;
import utils.PreferencesUtils;


/**
 * Created by sherihan
 */
public class SettingFragment extends BaseFragment {

    View rootView;

    Unbinder unbinder;

    @BindView(R.id.rViewLanguages)
    RecyclerView rViewLanguages ;

    @BindView(R.id.btnSave)
    Button btnSave ;

    LanguageAdapter languageAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_setting, container, false);

        unbinder = ButterKnife.bind(this, rootView);


        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(rootView, "fonts/JF-Flat-regular.ttf");



        languageAdapter=new LanguageAdapter(getContext());

        rViewLanguages.setHasFixedSize(true);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rViewLanguages.setLayoutManager(linearLayoutManager);

        Language languageArabic=new Language();
        languageArabic.setName(getString(R.string.setting_arabic));
        languageArabic.setLogo("arabic");

        Language languageEnglish=new Language();
        languageEnglish.setName(getString(R.string.setting_english));
        languageEnglish.setLogo("english");

        List<Language> languages=new ArrayList<>();
        languages.add(languageEnglish);
        languages.add(languageArabic);
        languageAdapter.AddMoreItems(languages);

        String str = Locale.getDefault().getDisplayLanguage();


        if(PreferencesUtils.getSharedPreference("Language", getContext()).equalsIgnoreCase("ar"))
        {
            languageArabic.setActive(true);
        }
        else
        {
            languageEnglish.setActive(true);
        }
        rViewLanguages.setAdapter(languageAdapter);


        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unbinder.unbind();
    }

    @OnClick(R.id.btnSave)
    void saveLanguage()
    {
        if(!languageAdapter.selectedLanguage.isEmpty() && languageAdapter.selectedLanguage.equalsIgnoreCase("arabic")) {
         //   LanguageChange.changeLang("ar", getContext());
            LanguageChange.saveLocale("ar", getContext());
        }
        else
        {
           // LanguageChange.changeLang("en", getContext());
            LanguageChange.saveLocale("en", getContext());
        }

        getActivity().setResult(getActivity().RESULT_OK);
        getActivity().finish();
    }

}
