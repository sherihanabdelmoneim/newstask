package freelancing.newstask.ui.customviews;

import android.app.Activity;
import android.support.v4.app.Fragment;

import cn.pedant.SweetAlert.SweetAlertDialog;
import freelancing.newstask.R;
import freelancing.newstask.ui.common.BaseActivity;

public class CustomAlertDialogs {




    public static void createErrorAlertDialog(final Activity activity, final Fragment dialogFragment, String message, final boolean finishActivity){

        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(activity.getString(R.string.app_name))
                .setContentText(message)
                .setConfirmText(activity.getString(R.string.txtOk))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                        if(finishActivity)
                        {
                            activity.finish();
                        }

                    }
                }).show();

    }


    public static void createSuccessAlertDialog(final BaseActivity activity, final Fragment dialogFragment, String title, String message, final boolean finishActivity){

        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(activity.getString(R.string.app_name))
                .setContentText(message)
                .setConfirmText(activity.getString(R.string.txtOk))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        if(finishActivity)
                        {
                            activity.finish();
                        }

                    }
                }).show();
    }




}
