package   freelancing.newstask.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import freelancing.newstask.R;
import freelancing.newstask.ui.common.BaseActivity;
import freelancing.newstask.ui.home.HomeActivity;
import me.anwarshahriar.calligrapher.Calligrapher;


public class SplashActivity extends BaseActivity {


    Unbinder unbinder;

    @BindView(R.id.imgLogo)
    ImageView imgLogo;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        unbinder = ButterKnife.bind(this);

        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/JF-Flat-regular.ttf", true);

        Animation objAnimation = AnimationUtils.loadAnimation(this, R.anim.splash_anim);
        objAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation anim) {


                Intent objIntent = new Intent(getApplicationContext(),HomeActivity.class);
                objIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(objIntent);
                finish();

            }
            public void onAnimationRepeat(Animation arg0) {}

            public void onAnimationStart(Animation arg0) {}
        });

        imgLogo.startAnimation(objAnimation);



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();

    }

}
