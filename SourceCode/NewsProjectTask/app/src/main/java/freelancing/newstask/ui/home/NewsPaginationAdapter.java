package freelancing.newstask.ui.home;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import freelancing.newstask.R;
import freelancing.newstask.dtos.ArticleDTO;
import freelancing.newstask.ui.NewDetails.NewsDetailsActivity;
import freelancing.newstask.ui.articlesource.ArticleSourceActivity;
import me.anwarshahriar.calligrapher.Calligrapher;
import utils.DateUtils;


/**
 * Created by sherihan
 */
public class NewsPaginationAdapter extends RecyclerView.Adapter<NewsPaginationAdapter.NewsHolder>  {

    private List<ArticleDTO> mlstArticles;
    Context context;



    public NewsPaginationAdapter(Context context, boolean isHistory)
    {

        mlstArticles = new ArrayList<ArticleDTO>();
        this.context=context;
    }

    public void AddMoreItems(List<ArticleDTO> lstWorkOrders)
    {
        //removeAllItems();
        mlstArticles.addAll(lstWorkOrders);
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        mlstArticles.removeAll(mlstArticles);
    }

    public List<ArticleDTO> getList()
    {
        return mlstArticles;
    }

    ////////////////////////////////////

    private NewsHolder setHolder(View objView) {
        NewsHolder objHolder = new NewsHolder(objView);



        return objHolder;
    }


    /////////////////////////////////////

    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int resource = R.layout.single_news;

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(resource, parent, false);

        Calligrapher calligrapher = new Calligrapher(context);
        calligrapher.setFont(itemView, "fonts/JF-Flat-regular.ttf");

        return  setHolder(itemView);

    }


    /////////////////////////////////////////////

    private void setData(ArticleDTO newDTO, NewsHolder objHolder, int i) {

        NewsHolder newsHolder = objHolder;
        newsHolder.articleDTO = newDTO;
        newsHolder.txtTitle.setText(newDTO.getTitle());

        if(newDTO.getSource() != null && newDTO.getSource().getName()!= null)
        {
            newsHolder.txtSource.setText(newDTO.getSource().getName());
        }


        if(newDTO.getUrl() != null && !newDTO.getUrl().isEmpty())
        {
            SpannableString content = new SpannableString(newDTO.getUrl());
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            newsHolder.txtReadMore.setText(content);
        }


        if(newDTO.getPublishedAt() != null && !newDTO.getPublishedAt().isEmpty())
        {
            newsHolder.txtDate.setText(DateUtils.formatTimestamp(newDTO.getPublishedAt()));
        }

        if(newDTO.getAuthor() != null && !newDTO.getAuthor().isEmpty())
        {
            newsHolder.txtAuthor.setVisibility(View.VISIBLE);
            newsHolder.txtRuler.setVisibility(View.VISIBLE);
            newsHolder.txtAuthor.setText(newDTO.getAuthor());

        }
        else
        {
            newsHolder.txtAuthor.setVisibility(View.GONE);
            newsHolder.txtRuler.setVisibility(View.GONE);
        }

        Glide.with(context).
                load(newDTO.getUrlToImage())
                .thumbnail(0.5f)
                .into(newsHolder.imgNews);


    }


    /////////////////////////////////////////////

    @Override
    public void onBindViewHolder(NewsHolder holder, int position) {

        ArticleDTO articleDTO = mlstArticles.get(position);
        setData(articleDTO,holder,position);


    }

    @Override
    public int getItemCount() {
        if(mlstArticles !=null)
        {
            return mlstArticles.size();
        }
        else
        {
            return 0;
        }
    }



    public class NewsHolder extends RecyclerView.ViewHolder {

        private ArticleDTO articleDTO;
        ImageView imgHeart;
        ImageView imgNews;
        TextView txtTitle;
        TextView txtDate;
        TextView txtReadMore;
        TextView txtSource;
        TextView txtRuler;
        TextView txtAuthor;



        public NewsHolder(View itemView) {
            super(itemView);

            imgHeart= itemView.findViewById(R.id.imgHeart);
            imgNews= itemView.findViewById(R.id.imgNews);
            txtTitle= itemView.findViewById(R.id.txtTitle);
            txtDate= itemView.findViewById(R.id.txtDate);
            txtReadMore= itemView.findViewById(R.id.txtReadMore);
            txtSource= itemView.findViewById(R.id.txtSource);
            txtRuler= itemView.findViewById(R.id.txtRuler);
            txtAuthor= itemView.findViewById(R.id.txtAuthor);


            imgNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent objIntent = new Intent(context,NewsDetailsActivity.class);
                    objIntent.putExtra("news",Parcels.wrap(articleDTO));
                    context.startActivity(objIntent);
                }
            });

            imgHeart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(imgHeart.getDrawable().getConstantState().equals(context.getDrawable(R.drawable.heart).getConstantState()))
                    {
                        imgHeart.setImageDrawable(context.getDrawable(R.drawable.unheart));

                    }
                    else
                    {
                        imgHeart.setImageDrawable(context.getDrawable(R.drawable.heart));

                    }

                }
            });

            txtReadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent objIntent = new Intent(context, ArticleSourceActivity.class);
                    objIntent.putExtra("Source",articleDTO.getUrl());
                    context.startActivity(objIntent);
                }
            });


        }



    }




}
