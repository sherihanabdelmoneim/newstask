package freelancing.newstask.ui.home;


import freelancing.newstask.dtos.NewsRequestDTO;
import freelancing.newstask.dtos.NewsResponseDTO;
import freelancing.newstask.interactors.home.HomeInteractor;
import freelancing.newstask.interactors.home.HomeInteractorImpl;


/**
 * Created by Sherihan
 */
public class HomePresenterImpl implements HomePresenter, HomeInteractor.OnHomeFinished {


    HomeView homeView ;
    private HomeInteractor homeInteractor;

    public HomePresenterImpl(HomeView homeView){

        this.homeView = homeView ;
        homeInteractor = new HomeInteractorImpl();
    }




    @Override
    public void onSuccessGetNews(NewsResponseDTO newsResponseDTO) {
        homeView.hideLoading();
        homeView.onSuccessGetNews(newsResponseDTO);
    }

    @Override
    public void onSuccessGetHeadLines(NewsResponseDTO newsResponseDTO) {
        homeView.hideLoading();
        homeView.onSuccessGetHeadLines(newsResponseDTO);
    }

    @Override
    public void onError(String message) {

        homeView.hideLoading();
        homeView.showError(message);

    }

    @Override
    public void onGenericError() {
        homeView.hideLoading();
        homeView.showGenericError();
    }


    @Override
    public void getNews(NewsRequestDTO newsRequestDTO) {
        homeView.showLoading();
        homeInteractor.getNews(newsRequestDTO,this);
    }

    @Override
    public void getHeadlines() {
        homeView.showLoading();
        homeInteractor.getHeadlines(this);
    }

    @Override
    public void onDestroy() {

        homeView = null ;
        homeInteractor.unSubscribeAll();
    }
}
