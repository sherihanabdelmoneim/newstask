package freelancing.newstask.data.api;


import freelancing.newstask.dtos.NewsResponseDTO;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by Sherihan
 */
public interface HomeAPI {


    @GET("everything?sortBy=publishedAt")
    Observable<NewsResponseDTO> getNews(@Query("q") String searchText,@Query("language") String langauge,@Query("from") String from,@Query("apiKey") String apiKey );

    @GET("top-headlines?sources=bbc-news")
    Observable<NewsResponseDTO> getHeadLines(@Query("apiKey") String apiKey);

}
