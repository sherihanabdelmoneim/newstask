package freelancing.newstask.ui.home;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import freelancing.newstask.R;
import freelancing.newstask.ui.common.BaseFragment;
import me.anwarshahriar.calligrapher.Calligrapher;


/**
 * Created by sherihan
 */
public class HomeFragment extends BaseFragment {


    View rootView ;
    Unbinder unbinder;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout ;

    ViewPagerAdapter adapter;





    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_home, container , false);

        unbinder = ButterKnife.bind(this,rootView);

        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(rootView, "fonts/JF-Flat-regular.ttf");



        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();



        return rootView ;


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void setupTabIcons() {

        tabLayout.getTabAt(0).setText(getString(R.string.menu_main));
        tabLayout.getTabAt(1).setText(getString(R.string.menu_video));
        tabLayout.getTabAt(2).setText(getString(R.string.menu_different));
        tabLayout.getTabAt(3).setText(getString(R.string.menu_opinion));
        tabLayout.getTabAt(4).setText(getString(R.string.menu_favourite));

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab,null);
            Typeface fontFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/JF-Flat-regular.ttf");

            tv.setTypeface(fontFace);
            tabLayout.getTabAt(i).setCustomView(tv);
        }




    }


    private void setupViewPager(ViewPager viewPager) {


        adapter = new ViewPagerAdapter(getChildFragmentManager());

        adapter.addFragment(new NewsFragment(), "ONE");
        adapter.addFragment(new FavouriteFragment(), "ONE");
        adapter.addFragment(new FavouriteFragment(), "ONE");
        adapter.addFragment(new FavouriteFragment(), "ONE");
        adapter.addFragment(new FavouriteFragment(), "ONE");

        viewPager.setAdapter(adapter);


    }



    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }


    }

}
