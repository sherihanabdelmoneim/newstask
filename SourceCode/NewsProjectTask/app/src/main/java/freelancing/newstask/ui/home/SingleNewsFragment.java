package freelancing.newstask.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import freelancing.newstask.R;
import freelancing.newstask.dtos.ArticleDTO;
import freelancing.newstask.ui.NewDetails.NewsDetailsActivity;
import me.anwarshahriar.calligrapher.Calligrapher;


public class SingleNewsFragment extends Fragment {

    View rootView;
    Unbinder unbinder;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @BindView(R.id.imgNews)
    ImageView imgNews;


    ArticleDTO newDTO;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_single_news, container, false);
        unbinder = ButterKnife.bind(this,rootView);

        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(rootView, "fonts/JF-Flat-regular.ttf");

        newDTO = Parcels.unwrap(getArguments().getParcelable("news"));

        if(newDTO != null) {

            txtTitle.setText(newDTO.getTitle());

            Glide.with(getActivity()).
                    load(newDTO.getUrlToImage())
                    .thumbnail(0.5f)
                    .into(imgNews);
        }

        return rootView;
    }


    @OnClick(R.id.layMain)
    void gotoDetails()
    {
        Intent objIntent = new Intent(getActivity(),NewsDetailsActivity.class);
        objIntent.putExtra("news",Parcels.wrap(newDTO));
        startActivity(objIntent);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        unbinder.unbind();

    }
}
