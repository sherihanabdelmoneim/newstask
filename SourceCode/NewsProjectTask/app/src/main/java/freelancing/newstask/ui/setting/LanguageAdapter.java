package freelancing.newstask.ui.setting;


import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import freelancing.newstask.R;
import freelancing.newstask.dtos.Language;
import me.anwarshahriar.calligrapher.Calligrapher;


/**
 * Created by sherihan
 */
public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.LanguageHolder> {

    private List<Language> mlstLanguages;
    Context mContext;
    public String selectedLanguage="";
    private  ImageView lastOk = null;




    public LanguageAdapter(Context context) {

        mlstLanguages = new ArrayList<Language>();
        mContext = context;
    }

    public List<Language> getList() {
        return mlstLanguages;
    }

    public void AddMoreItems(List<Language> lstLangs) {
        removeAllItems();
        mlstLanguages.addAll(lstLangs);
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        mlstLanguages.removeAll(mlstLanguages);
    }


    ////////////////////////////////////

    private LanguageHolder setHolder(View objView) {
        LanguageHolder objHolder = new LanguageHolder(objView);

        return objHolder;
    }


    /////////////////////////////////////

    @Override
    public LanguageHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.single_language, parent, false);

        Calligrapher calligrapher = new Calligrapher(mContext);
        calligrapher.setFont(itemView, "fonts/JF-Flat-regular.ttf");


        return setHolder(itemView);

    }


    /////////////////////////////////////////////

    private void setData(final Language language, LanguageHolder objHolder, int i) {

        final LanguageHolder languageHolder = objHolder;
        languageHolder.setBusinessObject(language);

        languageHolder.txtLanguage.setText(language.getName());

        int imageId =mContext.getResources().getIdentifier(language.getLogo(), "drawable", mContext.getPackageName());

        languageHolder.imgLanguage.setImageResource(imageId);

        if(language.isActive()) {
            languageHolder.imgOk.setVisibility(View.VISIBLE);
            lastOk = languageHolder.imgOk;

            Animation pulsecar_accedent = AnimationUtils.loadAnimation(mContext, R.anim.language_pulse);
            languageHolder.imgOk.startAnimation(pulsecar_accedent);
        }
        else
        {
            languageHolder.imgOk.setVisibility(View.GONE);
        }


    }



    /////////////////////////////////////////////

    @Override
    public void onBindViewHolder(LanguageHolder holder, int position) {

        Language language = mlstLanguages.get(position);
        setData(language, holder, position);

    }

    @Override
    public int getItemCount() {
        if (mlstLanguages != null) {
            return mlstLanguages.size();
        } else {
            return 0;
        }
    }



    public class LanguageHolder extends RecyclerView.ViewHolder
    {
        private Language language;
        private CardView card_view;
        private TextView txtLanguage;
        private ImageView imgLanguage;
        private ImageView imgOk;


        public LanguageHolder(View itemView) {

            super(itemView);

            card_view = (CardView)itemView.findViewById(R.id.card_view);
            txtLanguage= (TextView) itemView.findViewById(R.id.txtLanguage);
            imgLanguage= (ImageView) itemView.findViewById(R.id.imgLanguage);
            imgOk= (ImageView) itemView.findViewById(R.id.imgOk);


            card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(lastOk != null)
                    {
                        lastOk.setVisibility(View.GONE);
                    }
                    imgOk.setVisibility(View.VISIBLE);
                    lastOk = imgOk;
                    selectedLanguage = language.getName();

                    Animation pulsecar_accedent = AnimationUtils.loadAnimation(mContext, R.anim.language_pulse);
                    imgOk.startAnimation(pulsecar_accedent);
                }
            });



        }

        public Language getBusinessObject()
        {
            return language;
        }

        public void setBusinessObject(Language language)
        {
            this.language = language;
        }

    }




}
