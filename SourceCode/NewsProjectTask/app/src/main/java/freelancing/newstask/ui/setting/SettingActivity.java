package freelancing.newstask.ui.setting;


import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import freelancing.newstask.R;
import freelancing.newstask.ui.common.BaseActivity;
import me.anwarshahriar.calligrapher.Calligrapher;


/**
 * Created by sherihan
 */
public class SettingActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.details_activity);

        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/JF-Flat-regular.ttf", true);


        setUpToolBar("Settings");

        startFragment();

    }


    private void setUpToolBar(String title){
        //add the Toolbar
        Toolbar toolbar= (Toolbar) findViewById(R.id.common_toolbar);

        ((TextView)toolbar.findViewById(R.id.txtTitle)).setText(title);


        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //do something you want
                finish();
            }
        });
    }

    private void startFragment() {

        SettingFragment settingFragment = new SettingFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.layCommonActivity, settingFragment).commit();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

}
