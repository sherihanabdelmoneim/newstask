package freelancing.newstask.ui.NewDetails;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.ScrollingMovementMethod;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import freelancing.newstask.R;
import freelancing.newstask.dtos.ArticleDTO;
import me.anwarshahriar.calligrapher.Calligrapher;
import utils.DateUtils;


public class NewsDetailsFragment extends Fragment {

    View rootView;
    Unbinder unbinder;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @BindView(R.id.txtDate)
    TextView txtDate;

    @BindView(R.id.txtSource)
    TextView txtSource;

    @BindView(R.id.txtReadMore)
    TextView txtReadMore;

    @BindView(R.id.imgNews)
    ImageView imgNews;

    @BindView(R.id.txtDescription)
    TextView txtDescription;

    ArticleDTO articleDTO;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_details, container, false);

        unbinder = ButterKnife.bind(this,rootView);

        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(rootView, "fonts/JF-Flat-regular.ttf");

        articleDTO = Parcels.unwrap(getActivity().getIntent().getParcelableExtra("news"));


        if(articleDTO != null) {

            txtTitle.setText(articleDTO.getTitle());

            txtDate.setText(DateUtils.formatTimestamp(articleDTO.getPublishedAt()));

            txtDescription.setMovementMethod(new ScrollingMovementMethod());

            if(articleDTO.getSource() != null && articleDTO.getSource().getName()!= null)
            {
                txtSource.setText(articleDTO.getSource().getName());

            }

            if(articleDTO.getContent() != null && !articleDTO.getContent().isEmpty()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    txtDescription.setText(Html.fromHtml(articleDTO.getContent()
                            , Html.FROM_HTML_MODE_COMPACT));
                } else {
                    txtDescription.setText(articleDTO.getContent());
                }
            }

            if(articleDTO.getUrl() != null && !articleDTO.getUrl().isEmpty())
            {
                SpannableString content = new SpannableString(articleDTO.getUrl());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                txtReadMore.setText(content);
            }

            Glide.with(getActivity()).
                    load(articleDTO.getUrlToImage())
                    .thumbnail(0.5f)
                    .into(imgNews);
        }

        return rootView;
    }




    @Override
    public void onDestroy() {
        super.onDestroy();

        unbinder.unbind();

    }
}
