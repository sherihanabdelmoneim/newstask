package freelancing.newstask.ui.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by Sherihan Abdelmoneim on 8/31/2016.
 */
public class NewsAdapter extends FragmentStatePagerAdapter {

    private List<SingleNewsFragment> fragments;

    public NewsAdapter(FragmentManager fm, List<SingleNewsFragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int pos) {
        return this.fragments.get(pos);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }
}
