package freelancing.newstask.ui.home;


import freelancing.newstask.dtos.NewsRequestDTO;

/**
 * Created by Sherihan
 */
public interface HomePresenter {


    void getNews(NewsRequestDTO newsRequestDTO);
    void getHeadlines();


    void onDestroy();
}
