package freelancing.newstask.ui;

/**
 * Created by sherihan
 */
public interface IView {

    void showLoading();

    void hideLoading();

    void showError(String message);

    void showInternetConnectionError();

    void showGenericError();

}
