package freelancing.newstask.ui.home;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.parceler.Parcels;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import freelancing.newstask.R;
import freelancing.newstask.dtos.ArticleDTO;
import freelancing.newstask.dtos.NewsRequestDTO;
import freelancing.newstask.dtos.NewsResponseDTO;
import freelancing.newstask.ui.UIConstants;
import freelancing.newstask.ui.common.BaseFragment;
import freelancing.newstask.ui.customviews.CustomAlertDialogs;
import me.anwarshahriar.calligrapher.Calligrapher;
import utils.PreferencesUtils;


/**
 * Created by sherihan
 */

public class NewsFragment extends BaseFragment implements HomeView {


    Unbinder unbinder;
    View rootView ;
    View container;

    @BindView(R.id.newsList)
    RecyclerView newsList;

    @BindView(R.id.layout_loading)
    View loadingLayout ;

    @BindView(R.id.empty_view)
    TextView empty_view;

    @BindView(R.id.newsPager)
    WrapContentViewPager  newsPager;


    @BindView(R.id.newsIndicator)
    CirclePageIndicator newsIndicator;

    private NewsPaginationAdapter paginationAdapter;

    private LinearLayoutManager layoutManager;


    boolean isLoading=false;
    NewsAdapter newsAdapter;

    int currentPage = 0;
    Timer timer;

    HomePresenter homePresenter;

    private final int VISIBLE_THRESHOLD = 1;
    private int lastVisibleItem, totalItemCount;
    int pageNumber = 0;
    boolean IsClear=false;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    Date currentDate = new Date();



    public NewsFragment()
    {


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.action_search);
        EditText searchEditText = (EditText) searchItem.getActionView().findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.activity_main_drawer, menu);
        super.onCreateOptionsMenu(menu,inflater);

        MenuItem mSearch = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) mSearch.getActionView();
        searchView.setIconifiedByDefault(true);
        searchView.setIconified(false);

        MenuItemCompat.setOnActionExpandListener(mSearch, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                TextView txtTitle = getActivity().findViewById(R.id.txtTitle);
                txtTitle.setVisibility(View.VISIBLE);
                return true;
            }
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if(newText.length() >= 3)
                {
                    if(checkInternetConnection())
                    {
                        isLoading=true;
                        IsClear= true;
                        pageNumber= 0;
                        homePresenter.getNews(new NewsRequestDTO(newText,PreferencesUtils.getSharedPreference("Language",getActivity()),formatter.format(new Date()),pageNumber));

                    }
                    else
                    {
                        showInternetConnectionError();
                    }
                }


                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView txtTitle = getActivity().findViewById(R.id.txtTitle);
                txtTitle.setVisibility(View.GONE);

            }
        });




    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        paginationAdapter = new NewsPaginationAdapter(getContext(),false);
        homePresenter=new HomePresenterImpl(this);

        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_news, container , false);
        unbinder = ButterKnife.bind(this,rootView);
        this.container = container;

        Calligrapher calligrapher = new Calligrapher(getActivity());
        calligrapher.setFont(rootView, "fonts/JF-Flat-regular.ttf");

        newsList.setVisibility(View.GONE);
        empty_view.setVisibility(View.VISIBLE);

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        newsList.setLayoutManager(layoutManager);
        newsList.setAdapter(paginationAdapter);


        setUpLoadMoreListener();

        paginationAdapter.removeAllItems();
        paginationAdapter.notifyDataSetChanged();

        if(checkInternetConnection())
        {
            isLoading=true;

            homePresenter.getNews(new NewsRequestDTO("sport",PreferencesUtils.getSharedPreference("Language",getActivity()),formatter.format(currentDate),pageNumber));
            homePresenter.getHeadlines();

        }
        else
        {
            showInternetConnectionError();
        }

        return rootView;
    }


    private void setUpLoadMoreListener() {

        newsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                if(!isLoading &&totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD))
                {

                    if(checkInternetConnection())
                    {
                        pageNumber++;
                        isLoading=true;
                        currentDate =new DateTime(currentDate).minusDays(1).toDate();
                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<< "+currentDate);
                        homePresenter.getNews(new NewsRequestDTO("sport",PreferencesUtils.getSharedPreference("Language",getActivity()),formatter.format(currentDate),pageNumber));

                    }
                    else
                    {
                        showInternetConnectionError();
                    }
                }
            }
        });

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && getFragmentManager() != null) {
            getFragmentManager().popBackStack();
        }

        if(isVisibleToUser)
        {
            currentPage = 0;
        }
    }

    @Override
    public void onDestroyView() {

        if(homePresenter != null) {
            homePresenter.onDestroy();
        }
        unbinder.unbind();
        super.onDestroyView();

    }

    @Override
    public void showLoading() {

        loadingLayout.setVisibility(View.VISIBLE);
        empty_view.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {

        loadingLayout.setVisibility(View.GONE);
        newsList.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String message) {
        CustomAlertDialogs.createErrorAlertDialog(getActivity(),null,message,false);

    }

    @Override
    public void showInternetConnectionError() {
        Snackbar.make(container,getString(R.string.connection_error), Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void showGenericError() {
        CustomAlertDialogs.createErrorAlertDialog(getActivity(),null,getString(R.string.error_Message),false);

    }


    @Override
    public void onSuccessGetNews(NewsResponseDTO newsResponseDTO) {

        isLoading = false;

        if (newsResponseDTO.getStatus() != null && !newsResponseDTO.getStatus().isEmpty() && newsResponseDTO.getStatus().equalsIgnoreCase("ok")) {
            if (newsResponseDTO.getArticles() != null && newsResponseDTO.getArticles().size() > 0) {
                if (IsClear) {
                    paginationAdapter.removeAllItems();
                    IsClear = false;
                }

                paginationAdapter.AddMoreItems(newsResponseDTO.getArticles());
            }


            if (paginationAdapter.getList().isEmpty()) {
                newsList.setVisibility(View.GONE);
                empty_view.setVisibility(View.VISIBLE);
            } else {
                newsList.setVisibility(View.VISIBLE);
                empty_view.setVisibility(View.GONE);
            }


        }
    }


    @Override
    public void onSuccessGetHeadLines(NewsResponseDTO newsResponseDTO) {

        List<SingleNewsFragment> fragments=new ArrayList<SingleNewsFragment>();

        int intCount = 5;
        if(newsResponseDTO.getArticles().size() < 5)
        {
            intCount = newsResponseDTO.getArticles().size();
        }

        for (int intIndex = 0;intIndex < intCount;intIndex++)
        {

            SingleNewsFragment singleNewsFragment =new SingleNewsFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("news",Parcels.wrap(newsResponseDTO.getArticles().get(intIndex)));
            singleNewsFragment.setArguments(bundle);
            fragments.add(singleNewsFragment);
        }

        newsAdapter = new NewsAdapter(getChildFragmentManager(), fragments);
        newsPager.setAdapter(newsAdapter);
        newsIndicator.setViewPager(newsPager);


        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == newsAdapter.getCount()-1) {
                    currentPage = 0;
                }
                else {
                    currentPage++;
                }
                if(newsPager != null) {
                    newsPager.setCurrentItem(currentPage, true);
                }
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, UIConstants.DELAY_MS, UIConstants.PERIOD_MS);

        if(newsPager != null) {
            newsPager.setCurrentItem(currentPage, true);
        }

    }

}
