package freelancing.newstask.ui.common;

import android.support.v4.app.Fragment;

import utils.ConnectionUtils;


public class BaseFragment extends Fragment {


    public boolean checkInternetConnection(){

        return ConnectionUtils.hasInternet(getActivity());
    }


}
