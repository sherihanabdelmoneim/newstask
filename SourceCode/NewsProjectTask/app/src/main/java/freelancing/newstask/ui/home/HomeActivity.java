package freelancing.newstask.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import freelancing.newstask.R;
import freelancing.newstask.ui.UIConstants;
import freelancing.newstask.ui.common.BaseActivity;
import freelancing.newstask.ui.setting.SettingActivity;
import me.anwarshahriar.calligrapher.Calligrapher;


/**
 * Created by sherihan.
 */
public class HomeActivity extends BaseActivity {


    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;


    @BindView(R.id.imgSettings)
    ImageView imgSettings;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);


        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/JF-Flat-regular.ttf", true);

        setUpToolBar();



        startFragment();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void setUpToolBar(){
        //add the Toolbar
        Toolbar toolbar=  findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(false);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //do something you want

                finish();
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle=new ActionBarDrawerToggle(this,drawer_layout,toolbar,R.string.app_name, R.string.app_name);
        drawer_layout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();



    }


    private void startFragment() {

        HomeFragment homeFragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.content_frame, homeFragment).commit();
    }

    @OnClick(R.id.imgSettings)
    void goToSettings()
    {
        startActivityForResult(new Intent(this, SettingActivity.class), UIConstants.LANGUAGE_SETTINGS);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == UIConstants.LANGUAGE_SETTINGS)
        {
            finish();
            startActivity(getIntent());
        }
    }

}
