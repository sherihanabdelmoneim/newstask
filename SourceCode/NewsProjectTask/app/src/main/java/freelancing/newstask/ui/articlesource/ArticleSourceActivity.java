package freelancing.newstask.ui.articlesource;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import freelancing.newstask.R;
import freelancing.newstask.ui.NewDetails.NewsDetailsFragment;
import freelancing.newstask.ui.common.BaseActivity;
import me.anwarshahriar.calligrapher.Calligrapher;


/**
 * Created by sherihan.
 */
public class ArticleSourceActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.source_activity);

        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/JF-Flat-regular.ttf", true);

      //  setUpToolBar();
        startFragment();
    }

    private void setUpToolBar(){
        //add the Toolbar
        Toolbar toolbar= (Toolbar) findViewById(R.id.common_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //do something you want

                finish();
            }
        });



    }


    private void startFragment() {

        ArticleSourceFragment articleSourceFragment = new ArticleSourceFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.layCommonActivity, articleSourceFragment).commit();
    }

}
