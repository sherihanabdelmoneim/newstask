package freelancing.newstask.dtos;

/**
 * Created by Sherihan Abdelmoneim on 8/12/2017.
 */

public class Language {

    private String name;
    private String logo;
    private boolean active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
