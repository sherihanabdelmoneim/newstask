package freelancing.newstask.interactors.home;


import freelancing.newstask.dtos.NewsRequestDTO;
import freelancing.newstask.dtos.NewsResponseDTO;


/**
 * Created by sherihan
 */
public interface HomeInteractor {

    interface OnHomeFinished {

        void onSuccessGetNews(NewsResponseDTO newsResponseDTO);
        void onSuccessGetHeadLines(NewsResponseDTO newsResponseDTO);
        void onError(String message);
        void onGenericError();

    }

    void getNews(NewsRequestDTO newsRequestDTO, OnHomeFinished onHomeFinished);
    void getHeadlines(OnHomeFinished onHomeFinished);

    void unSubscribeAll();
}
