package freelancing.newstask.dtos;

import java.util.List;

public class NewsResponseDTO {
    private String status;
    private int totalResults;
    private List<ArticleDTO> articles;
    private String message;
    private String code;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public List<ArticleDTO> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleDTO> articles) {
        this.articles = articles;
    }
}
