package freelancing.newstask.ui.articlesource;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import freelancing.newstask.R;


public class ArticleSourceFragment extends Fragment {

    Unbinder unbinder;
    View rootView ;

    @BindView(R.id.wViewArticleSource)
    WebView wViewArticleSource;

    @BindView(R.id.layout_loading)
    View loadingLayout ;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_article_source, container, false);

        unbinder = ButterKnife.bind(this,rootView);

        String url= getActivity().getIntent().getStringExtra("Source");

        wViewArticleSource.getSettings().setJavaScriptEnabled(true);
        wViewArticleSource.getSettings().setDomStorageEnabled(true);
        wViewArticleSource.getSettings().setLoadWithOverviewMode(true);
        wViewArticleSource.getSettings().setUseWideViewPort(true);
        wViewArticleSource.setWebViewClient(new SwAWebClient());



        wViewArticleSource.loadUrl(url);

        return rootView;

    }

    private class SwAWebClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            System.out.println("########################################## URL "+url);

            view.loadUrl(url);


            return true;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideLoading();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showLoading();
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }


    }

    private void showLoading()
    {
        loadingLayout.setVisibility(View.VISIBLE);
    }

    private void hideLoading()
    {
        loadingLayout.setVisibility(View.GONE);
    }




}
