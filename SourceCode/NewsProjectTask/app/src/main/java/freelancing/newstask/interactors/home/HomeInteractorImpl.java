package freelancing.newstask.interactors.home;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import org.reactivestreams.Publisher;

import java.io.IOException;

import freelancing.newstask.data.ServiceGenerator;
import freelancing.newstask.data.api.HomeAPI;
import freelancing.newstask.dtos.ErrorDTO;
import freelancing.newstask.dtos.NewsRequestDTO;
import freelancing.newstask.dtos.NewsResponseDTO;
import freelancing.newstask.ui.UIConstants;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.HttpException;


/**
 * Created by sherihan
 */
public class HomeInteractorImpl implements HomeInteractor{


    HomeAPI homeAPI;
    CompositeDisposable mCompositeSubscription ;

    public HomeInteractorImpl(){
        mCompositeSubscription = new CompositeDisposable();
        homeAPI = new ServiceGenerator().createService(HomeAPI.class);

    }

    @Override
    public void getNews(final NewsRequestDTO newsRequestDTO, final OnHomeFinished onHomeFinished) {


        PublishProcessor<Integer> paginator = PublishProcessor.create();

        Disposable disposable = paginator.onBackpressureDrop()
                .concatMap(new Function<Integer, Publisher<NewsResponseDTO>>() {
                    @Override
                    public Publisher<NewsResponseDTO> apply(@NonNull Integer page) throws Exception {

                        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<page number"+page);

                        return getNewsPaging(newsRequestDTO,onHomeFinished);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<NewsResponseDTO>() {
                    @Override
                    public void accept(@NonNull NewsResponseDTO items) throws Exception {
                        System.out.println("************************Return Here *****************************8");
                        onHomeFinished.onSuccessGetNews(items);
                    }
                });

        mCompositeSubscription.add(disposable);

        paginator.onNext(newsRequestDTO.getPageNumber());



    }

    @Override
    public void getHeadlines(final  OnHomeFinished onHomeFinished) {
         Observable<NewsResponseDTO> observable = homeAPI.getHeadLines(UIConstants.API_KEY);

        mCompositeSubscription.add(observable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<NewsResponseDTO>() {
                    @Override
                    public void onComplete()
                    {
                    }

                    @Override
                    public void onNext(NewsResponseDTO newsResponseDTO)
                    {
                        onHomeFinished.onSuccessGetHeadLines(newsResponseDTO);
                    }

                    @Override
                    public void onError(Throwable exobject)
                    {
                        if (exobject instanceof HttpException) {

                        ResponseBody body = ((HttpException)exobject).response().errorBody();


                        Gson gson = new Gson();
                        TypeAdapter<ErrorDTO> adapter = gson.getAdapter
                                (ErrorDTO
                                        .class);
                        try {
                            ErrorDTO errorParser =
                                    adapter.fromJson(body.string());

                            System.out.println("=======================error " + errorParser.getMessage());

                            onHomeFinished.onError(errorParser.getMessage());


                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                }
                else
                {
                    onHomeFinished.onGenericError();
                }


                }}));
    }


    private Flowable<NewsResponseDTO> getNewsPaging(NewsRequestDTO newsRequestDTO, final OnHomeFinished onHomeFinished) {

        final Observable<NewsResponseDTO> Obr=homeAPI.getNews(newsRequestDTO.getSearhText(),newsRequestDTO.getLanguage(),newsRequestDTO.getFrom(), UIConstants.API_KEY);

        mCompositeSubscription.add(Obr.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribeWith(new DisposableObserver<NewsResponseDTO>() {
                    @Override
                    public void onNext(NewsResponseDTO workOrderDTOs) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {

                            ResponseBody body = ((HttpException) e).response().errorBody();


                            Gson gson = new Gson();
                            TypeAdapter<ErrorDTO> adapter = gson.getAdapter
                                    (ErrorDTO
                                            .class);
                            try {
                                ErrorDTO errorParser =
                                        adapter.fromJson(body.string());

                                System.out.println("=======================error " + errorParser.getMessage());

                                onHomeFinished.onError(errorParser.getMessage());


                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onComplete()
                    {

                    }
                }));

        final Flowable<NewsResponseDTO> flowable ;

        flowable = Obr.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .map(new Function<NewsResponseDTO, NewsResponseDTO>() {
                    @Override
                    public NewsResponseDTO apply(@NonNull NewsResponseDTO workOrderResponseDTO) throws Exception {


                        return workOrderResponseDTO;
                    }
                }).onErrorReturn(new Function<Throwable, NewsResponseDTO>() {
                    @Override
                    public NewsResponseDTO apply(@NonNull Throwable throwable) throws Exception {
                        return new NewsResponseDTO();
                    }
                }).toFlowable(BackpressureStrategy.ERROR);


        return flowable;

    }


    @Override
    public void unSubscribeAll() {
        if(null !=mCompositeSubscription && !mCompositeSubscription.isDisposed()) {
            mCompositeSubscription.clear();
            mCompositeSubscription.dispose();
        }
    }


}
