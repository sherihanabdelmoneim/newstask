package utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;

import java.util.Locale;

/**
 * Created by Sherihan Abdelmoneim on 10/14/2016.
 */
public class LanguageChange {

    public static void changeLang(String lang, Context context)
    {
        if (lang.equalsIgnoreCase(""))
            return;


        Locale newLocale = new Locale(lang);
        Resources res = context.getResources();
        Configuration configuration = res.getConfiguration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocale(newLocale);

            LocaleList localeList = new LocaleList(newLocale);
            LocaleList.setDefault(localeList);
            configuration.setLocales(localeList);

            context = context.createConfigurationContext(configuration);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(newLocale);
            context = context.createConfigurationContext(configuration);
            Locale.setDefault(newLocale);

        } else {
            configuration.locale = newLocale;
            res.updateConfiguration(configuration, res.getDisplayMetrics());
            Locale.setDefault(newLocale);
        }
    }


    public static String getLanguage(Context context)
    {
        String lang= PreferencesUtils.getSharedPreference("Language", context);

        if(lang == null
                || lang.isEmpty())
        {
            lang= "en";
        }

        return lang;
    }

    public static void saveLocale(String lang, Context context)
    {
        String langPref = "Language";
        PreferencesUtils.saveToSharedPreferences(langPref,lang,context);

    }


    public static void loadLocale(Context context)
    {
        String langPref = "Language";
        changeLang(PreferencesUtils.getSharedPreference(langPref, context),context);
    }
}



