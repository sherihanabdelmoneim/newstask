package TwoP.task.ui.route;


import TwoP.task.dtos.RouteRequestDTO;

/**
 * Created by Sherihan
 */
public interface RoutePresenter {


    void getRoute(RouteRequestDTO routeRequestDTO);
    void getInfo();
    void onDestroy();
}
