package TwoP.task.dtos;

public class RouteResponseDTO {
    private String Message;
    private boolean Status;
    private  MainDataDTO InnerData;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public MainDataDTO getInnerData() {
        return InnerData;
    }

    public void setInnerData(MainDataDTO innerData) {
        InnerData = innerData;
    }
}
