package TwoP.task.interactors.route;


import TwoP.task.dtos.AboutUsResponseDTO;
import TwoP.task.dtos.RouteRequestDTO;
import TwoP.task.dtos.RouteResponseDTO;

/**
 * Created by sherihan
 */
public interface RouteInteractor {

    interface OnRouteFinished {

        void onSuccessGetRoute(RouteResponseDTO routeResponseDTO);
        void onSuccessGetInfo(AboutUsResponseDTO aboutUsResponseDTO);
        void onError(String message);
        void onGenericError();
    }

    void getRoute(RouteRequestDTO routeRequestDTO,OnRouteFinished onRouteFinished);
    void getInfo(OnRouteFinished onRouteFinished);
    void unSubscribeAll();
}
