package TwoP.task.dtos;

public class RouteRequestDTO {
    private String name;
    private String password;
    private String deviceToken;

    public RouteRequestDTO(String name,String password,String deviceToken)
    {
        this.name = name;
        this.password = password;
        this.deviceToken = deviceToken;
    }

}
