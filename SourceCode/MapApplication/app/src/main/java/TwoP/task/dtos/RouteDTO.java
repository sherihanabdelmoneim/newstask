package TwoP.task.dtos;

import java.util.List;

public class RouteDTO {

    private List<PointDTO> routePath;

    public List<PointDTO> getRoutePath() {
        return routePath;
    }

    public void setRoutePath(List<PointDTO> routePath) {
        this.routePath = routePath;
    }
}

