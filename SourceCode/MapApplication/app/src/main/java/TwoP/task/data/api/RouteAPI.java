package TwoP.task.data.api;


import TwoP.task.dtos.AboutUsResponseDTO;
import TwoP.task.dtos.RouteRequestDTO;
import TwoP.task.dtos.RouteResponseDTO;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by Sherihan
 */
public interface RouteAPI {


    @POST("account/checkCredentials")
    Observable<RouteResponseDTO> getRoute(@Body RouteRequestDTO routeRequestDTO);


    @GET("aboutus/aboutUs")
    Observable<AboutUsResponseDTO> getInfo();
}
