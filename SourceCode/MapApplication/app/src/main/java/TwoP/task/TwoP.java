package TwoP.task;

import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import utils.PreferencesUtils;

/**
 * Created by Sherihan Abdelmoneim on 3/23/2017.
 */

public class TwoP extends Application {


    private static String accessToken;



    public static void setAccessType(Context context) {
        accessToken= PreferencesUtils.getSharedPreference("TokenType", context);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        try
        {

            accessToken= PreferencesUtils.getSharedPreference("UserAccessToken", getApplicationContext());

        }
        catch (Throwable e)
        {
            throw new RuntimeException(e);
        }


    }


    public static String getAccessToken()
    {
        System.out.println("<<<<<<<<<<<<<<<<<getAccessToken<<<<<<<<<<<<<<<<<<<< "+accessToken);

        return accessToken;
    }

    public static void setAccessToken(Context context)
    {
        accessToken= PreferencesUtils.getSharedPreference("UserAccessToken", context);
    }



    public static boolean isRTL() {
        return isRTL(Locale.getDefault());
    }

    public static boolean isRTL(Locale locale) {
        final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
        return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT ||
                directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
    }
}
