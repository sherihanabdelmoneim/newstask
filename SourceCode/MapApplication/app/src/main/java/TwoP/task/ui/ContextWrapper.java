package TwoP.task.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;

import java.util.Locale;

/**
 * Created by Sherihan Abdelmoneim on 11/19/2017.
 */
public class ContextWrapper extends android.content.ContextWrapper {

        public ContextWrapper(Context base) {
                super(base);
        }

        public static ContextWrapper wrap(Context context, Locale newLocale) {


                Resources res = context.getResources();
                Configuration configuration = res.getConfiguration();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        configuration.setLocale(newLocale);

                        LocaleList localeList = new LocaleList(newLocale);
                        LocaleList.setDefault(localeList);
                        configuration.setLocales(localeList);

                        context = context.createConfigurationContext(configuration);

                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        configuration.setLocale(newLocale);
                        context = context.createConfigurationContext(configuration);
                        Locale.setDefault(newLocale);

                } else {
                        configuration.locale = newLocale;
                        res.updateConfiguration(configuration, res.getDisplayMetrics());
                        Locale.setDefault(newLocale);
                }

                return new ContextWrapper(context);


        }
}