package TwoP.task.dtos;

public class UserDTO {
    private BusDTO bus;

    public BusDTO getBus() {
        return bus;
    }

    public void setBus(BusDTO bus) {
        this.bus = bus;
    }
}
