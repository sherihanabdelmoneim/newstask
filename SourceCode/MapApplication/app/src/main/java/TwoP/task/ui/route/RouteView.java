package TwoP.task.ui.route;


import TwoP.task.dtos.AboutUsResponseDTO;
import TwoP.task.dtos.RouteResponseDTO;
import TwoP.task.ui.IView;


/**
 * Created by Sherihan
 */
public interface RouteView extends IView {

     void onSuccessGetRoute(RouteResponseDTO routeResponseDTO);
     void onSuccessGetInfo(AboutUsResponseDTO aboutUsResponseDTO);

}
