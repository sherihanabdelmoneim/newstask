package TwoP.task.interactors.route;


import TwoP.task.data.ServiceGenerator;
import TwoP.task.data.api.RouteAPI;
import TwoP.task.dtos.AboutUsResponseDTO;
import TwoP.task.dtos.RouteRequestDTO;
import TwoP.task.dtos.RouteResponseDTO;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by sherihan
 */
public class RouteInteractorImpl implements RouteInteractor{


    RouteAPI routeAPI;
    CompositeDisposable mCompositeSubscription ;

    public RouteInteractorImpl(){
        mCompositeSubscription = new CompositeDisposable();
        routeAPI = new ServiceGenerator().createService(RouteAPI.class);

    }


    @Override
    public void getRoute(RouteRequestDTO routeRequestDTO, final OnRouteFinished onRouteFinished) {

        Observable<RouteResponseDTO> observable = routeAPI.getRoute(routeRequestDTO);


        mCompositeSubscription.add(observable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<RouteResponseDTO>() {
                    @Override
                    public void onComplete()
                    {
                    }

                    @Override
                    public void onNext(RouteResponseDTO routeResponseDTO)
                    {
                        System.out.println("================== onNext ====================");

                        onRouteFinished.onSuccessGetRoute(routeResponseDTO);

                    }

                    @Override
                    public void onError(Throwable exobject)
                    {

                        System.out.println("==================onError==================== "+exobject.getLocalizedMessage());

                        onRouteFinished.onError(exobject.getLocalizedMessage());

                    }


                }));

    }

    @Override
    public void getInfo(final OnRouteFinished onRouteFinished) {
        Observable<AboutUsResponseDTO> observable = routeAPI.getInfo();


        mCompositeSubscription.add(observable.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<AboutUsResponseDTO>() {
                    @Override
                    public void onComplete()
                    {
                    }

                    @Override
                    public void onNext(AboutUsResponseDTO aboutUsResponseDTO)
                    {
                        System.out.println("================== onNext ====================");

                        onRouteFinished.onSuccessGetInfo(aboutUsResponseDTO);

                    }

                    @Override
                    public void onError(Throwable exobject)
                    {

                        System.out.println("==================onError==================== "+exobject.getLocalizedMessage());

                        onRouteFinished.onError(exobject.getLocalizedMessage());

                    }

                }));
    }

    @Override
    public void unSubscribeAll() {
        if(null !=mCompositeSubscription && !mCompositeSubscription.isDisposed()) {
            mCompositeSubscription.clear();
            mCompositeSubscription.dispose();
        }
    }


}
