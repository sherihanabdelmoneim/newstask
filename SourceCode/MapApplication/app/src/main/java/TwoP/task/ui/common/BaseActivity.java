package TwoP.task.ui.common;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import java.util.List;
import java.util.Locale;

import TwoP.task.ui.ContextWrapper;
import TwoP.task.ui.customviews.ProgressDialogFragment;
import utils.ConnectionUtils;
import utils.LanguageChange;


/**
 * Created by sherihan
 */
public class BaseActivity extends AppCompatActivity {



    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    ProgressDialogFragment mobjProgrss;

    public void showProgressDailog(String strProgressData)
    {
        if(mobjProgrss ==null)
        {
            mobjProgrss = new ProgressDialogFragment();
        }

        mobjProgrss.setMessage(strProgressData);
        mobjProgrss.setCancelable(false);

        if (!mobjProgrss.isVisible())
        {
            mobjProgrss.show(getSupportFragmentManager(), strProgressData);

        }
    }

    public void dismissProgressDailog()
    {
        if (mobjProgrss!=null)
        {
            mobjProgrss.dismissAllowingStateLoss();
        }
    }

    public boolean checkInternetConnection(){

        return ConnectionUtils.hasInternet(this);
    }



    @Override
    protected void attachBaseContext(Context newBase) {

        String language= LanguageChange.getLanguage(newBase);

        Context context = ContextWrapper.wrap(newBase, new Locale(language));
        super.attachBaseContext(context);


        System.out.println("+++++++++++++++++++++++++++++attachBaseContext++++++++++++++++++++++++++++++ "+ Locale.getDefault());

    }
}
