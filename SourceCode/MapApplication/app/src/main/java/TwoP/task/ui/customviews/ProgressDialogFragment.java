package TwoP.task.ui.customviews;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import TwoP.task.R;


/**
 * Created by sherihan
 */
public class ProgressDialogFragment extends DialogFragment {

    private String mstrMessage;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), getString(R.string.app_name), mstrMessage);
        return progressDialog;
    }

    public String getMessage() {
        return mstrMessage;
    }

    public void setMessage(String strMessage) {
        this.mstrMessage = strMessage;
    }
}
