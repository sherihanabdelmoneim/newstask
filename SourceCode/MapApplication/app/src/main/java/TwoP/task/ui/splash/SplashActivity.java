package TwoP.task.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import TwoP.task.R;
import TwoP.task.ui.common.BaseActivity;
import TwoP.task.ui.route.RouteActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;



public class SplashActivity extends BaseActivity {


    Unbinder unbinder;

    @BindView(R.id.imgLogo)
    ImageView imgLogo;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        unbinder = ButterKnife.bind(this);


        Animation objAnimation = AnimationUtils.loadAnimation(this, R.anim.splash_anim);
        objAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation anim) {


                Intent objIntent = new Intent(getApplicationContext(), RouteActivity.class);
                objIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(objIntent);
                finish();

            }
            public void onAnimationRepeat(Animation arg0) {}

            public void onAnimationStart(Animation arg0) {}
        });

        imgLogo.startAnimation(objAnimation);



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();

    }

}
