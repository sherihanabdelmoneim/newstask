package TwoP.task.ui.customviews;

import android.app.Activity;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

import TwoP.task.R;
import TwoP.task.ui.common.BaseActivity;
import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by mohamed on 28/06/16.
 */
public class CustomAlertDialogs {


    public static void createInputRequireAlertDialog(final BaseActivity activity, final Fragment dialogFragment, String title, String message, final boolean finishActivity){

        new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(activity.getString(R.string.app_name))
                .setContentText(message)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        if(finishActivity)
                        {
                            activity.finish();
                        }

                    }
                }).show();
    }

    public static void createErrorAlertDialog(final Activity activity, final Fragment dialogFragment, String message, final boolean finishActivity){

        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(activity.getString(R.string.app_name))
                .setContentText(message)
                .setConfirmText(activity.getString(R.string.txtOk))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismissWithAnimation();
                        if(finishActivity)
                        {
                            activity.finish();
                        }

                    }
                }).show();

    }


    public static void createSuccessAlertDialog(final BaseActivity activity, final Fragment dialogFragment, String title, String message, final boolean finishActivity){

        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(activity.getString(R.string.app_name))
                .setContentText(message)
                .setConfirmText(activity.getString(R.string.txtOk))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        if(finishActivity)
                        {
                            activity.finish();
                        }

                    }
                }).show();
    }

    public static void createSuccessAlertDismissDialog(final BaseActivity activity, final DialogFragment dialogFragment, String title, String message, final boolean finishActivity){

        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(activity.getString(R.string.app_name))
                .setContentText(message)
                .setConfirmText(activity.getString(R.string.txtOk))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        if(finishActivity)
                        {
                           dialogFragment.dismiss();
                        }

                    }
                }).show();
    }


}
