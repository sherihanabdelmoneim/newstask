package TwoP.task.ui.route;


import TwoP.task.dtos.AboutUsResponseDTO;
import TwoP.task.dtos.RouteRequestDTO;
import TwoP.task.dtos.RouteResponseDTO;
import TwoP.task.interactors.route.RouteInteractor;
import TwoP.task.interactors.route.RouteInteractorImpl;



/**
 * Created by Sherihan
 */
public class RoutePresenterImpl implements RoutePresenter, RouteInteractor.OnRouteFinished {


    RouteView routeView;
    private RouteInteractor routeInteractor;

    public RoutePresenterImpl(RouteView routeView){

        this.routeView = routeView;
        routeInteractor = new RouteInteractorImpl();
    }




    @Override
    public void onSuccessGetRoute(RouteResponseDTO routeResponseDTO) {
        routeView.hideLoading();
        routeView.onSuccessGetRoute(routeResponseDTO);
    }

    @Override
    public void onSuccessGetInfo(AboutUsResponseDTO aboutUsResponseDTO) {
        routeView.hideLoading();
        routeView.onSuccessGetInfo(aboutUsResponseDTO);
    }

    @Override
    public void onError(String message) {

        routeView.hideLoading();
        routeView.showError(message);

    }

    @Override
    public void onGenericError() {
        routeView.hideLoading();
        routeView.showGenericError();
    }




    @Override
    public void getRoute(RouteRequestDTO routeRequestDTO) {
        routeView.showLoading();
        routeInteractor.getRoute(routeRequestDTO,this);
    }

    @Override
    public void getInfo() {
        routeView.showLoading();
        routeInteractor.getInfo(this);
    }

    @Override
    public void onDestroy() {

        routeView = null ;
        routeInteractor.unSubscribeAll();
    }
}
