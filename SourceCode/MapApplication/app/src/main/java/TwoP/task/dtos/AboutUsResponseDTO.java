package TwoP.task.dtos;

import java.util.List;

public class AboutUsResponseDTO {
    private String Message;
    private boolean Status;
    private List<AboutUs> InnerData;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public List<AboutUs> getInnerData() {
        return InnerData;
    }

    public void setInnerData(List<AboutUs> innerData) {
        InnerData = innerData;
    }
}
