package TwoP.task.dtos;

public class BusDTO {
    private RouteDTO route;

    public RouteDTO getRoute() {
        return route;
    }

    public void setRoute(RouteDTO route) {
        this.route = route;
    }
}
