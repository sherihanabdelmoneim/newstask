package TwoP.task.data;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import TwoP.task.TwoP;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import utils.PreferencesUtils;

/**
 * Created by sherihan
 */

    public class ServiceGenerator {

   public static String API_BASE_URL = "http://inaclick.online/mtc/";

    OkHttpClient.Builder httpClient;
    Retrofit.Builder builder;

    public ServiceGenerator(){

        httpClient = new OkHttpClient.Builder().connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES);


        builder = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

    }


    // method to create any retrofit service
    public  <T> T createService(Class<T> serviceClass) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);


        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {


                System.out.println("=========================interceptors================================");
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder().method(original.method(), original.body());
                requestBuilder.addHeader("Content-Type", "application/json; text/html; charset=UTF-8");
                requestBuilder.addHeader("Authorization",
                        TwoP.getAccessToken());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        httpClient.addInterceptor(logging);


        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }

    }
