package TwoP.task.ui.route;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import TwoP.task.R;
import TwoP.task.dtos.AboutUsResponseDTO;
import TwoP.task.dtos.PointDTO;
import TwoP.task.dtos.RouteDTO;
import TwoP.task.dtos.RouteRequestDTO;
import TwoP.task.dtos.RouteResponseDTO;
import TwoP.task.ui.Config;
import TwoP.task.ui.common.BaseActivity;
import TwoP.task.ui.customviews.CustomAlertDialogs;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import utils.PreferencesUtils;

public class RouteActivity extends BaseActivity implements RouteView, OnMapReadyCallback {

    Unbinder unbinder;

    RoutePresenter routePresenter;

    GoogleMap googleMap;

    @BindView(R.id.layout_loading)
    View loadingLayout ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_route);

        unbinder = ButterKnife.bind(this);


        routePresenter = new RoutePresenterImpl(this);


        if(checkInternetConnection())
        {
            routePresenter.getRoute(new RouteRequestDTO(Config.USER_NAME,Config.PASSWORD,""));

        }
        else
        {
            showInternetConnectionError();
        }
    }

    @Override
    public void onSuccessGetRoute(RouteResponseDTO routeResponseDTO) {

        if(routeResponseDTO.isStatus())
        {
            //save token
            if(routeResponseDTO.getInnerData() != null)
            {
                PreferencesUtils.saveToSharedPreferences("UserAccessToken",routeResponseDTO.getInnerData().getToken(),this);

                AddToMapTask addToMapTask=new AddToMapTask();

                if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB)
                    addToMapTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,routeResponseDTO.getInnerData().getUser().getBus().getRoute());
                else
                    addToMapTask.execute(routeResponseDTO.getInnerData().getUser().getBus().getRoute());

            }
        }
        else
        {
            //show message error
            CustomAlertDialogs.createErrorAlertDialog(this,null,routeResponseDTO.getMessage(),false);

        }

    }

    @Override
    public void onSuccessGetInfo(AboutUsResponseDTO aboutUsResponseDTO) {

        DialogFragment baseDialogFragment = new InfoFragmentDialog();
        baseDialogFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Dialog_NoTitle);
        baseDialogFragment.setCancelable(false);

        if(aboutUsResponseDTO.getInnerData() != null && aboutUsResponseDTO.getInnerData().size() > 0)
        {
            Bundle bundle =new Bundle();
            bundle.putString("Content",aboutUsResponseDTO.getInnerData().get(0).getContent());
            baseDialogFragment.setArguments(bundle);
        }
        baseDialogFragment.show(getSupportFragmentManager(),"");
    }

    public void showLoading()
    {
        loadingLayout.setVisibility(View.VISIBLE);
    }

    public void hideLoading()
    {
        loadingLayout.setVisibility(View.GONE);
    }


    @Override
    public void showError(String message) {
        CustomAlertDialogs.createErrorAlertDialog(this,null,message,false);

    }

    @Override
    public void showInternetConnectionError() {
        Snackbar.make(loadingLayout,getString(R.string.connection_error), Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void showGenericError() {
        CustomAlertDialogs.createErrorAlertDialog(this,null,"An error has occured, please try again later",false);

    }

    @Override
    protected void onDestroy() {
            super.onDestroy();
        unbinder.unbind();
        routePresenter.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_view_fragment);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
    }

    @OnClick(R.id.imgInfo)
    void clickInfo()
    {
        if(checkInternetConnection())
        {
            routePresenter.getInfo();

        }
        else
        {
            showInternetConnectionError();
        }

    }


    private class AddToMapTask extends AsyncTask<RouteDTO,Void ,RouteDTO> {

        // Parsing the data in non-ui thread
        @Override
        protected RouteDTO doInBackground(RouteDTO... jsonData) {


            return jsonData[0];
        }

        @Override
        protected void onPostExecute(RouteDTO routeDTO) {

            List<LatLng> latLngs = new ArrayList<>();
            for (PointDTO pointDTO : routeDTO.getRoutePath())
            {
                latLngs.add(new LatLng(pointDTO.getLat(),pointDTO.getLng()));
            }

            PolylineOptions lineOptions = new PolylineOptions();
            lineOptions.addAll(latLngs);
            lineOptions.width(10);
            lineOptions.color(Color.parseColor("#e89922"));

            lineOptions.geodesic(true);
            googleMap.addPolyline(lineOptions);


            // Drawing polyline in the Google Map for the i-th route
            for (PointDTO pointDTO : routeDTO.getRoutePath())
            {
                MarkerOptions marker = new MarkerOptions()
                        .position(new LatLng(pointDTO.getLat(),pointDTO.getLng()));
               googleMap.addMarker(marker);
            }

            //for zooming
            if(latLngs.size() > 0)
            {

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (LatLng markerTmp : latLngs) {
                    builder.include(markerTmp);
                }
                LatLngBounds bounds = builder.build();

                int padding = 200; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                googleMap.moveCamera(cu);
                googleMap.animateCamera(cu);
            }

        }
    }
}
