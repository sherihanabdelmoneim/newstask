package TwoP.task.ui.route;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.sufficientlysecure.htmltextview.HtmlResImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.List;

import TwoP.task.R;
import TwoP.task.dtos.AboutUsResponseDTO;
import TwoP.task.dtos.RouteResponseDTO;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import utils.ConnectionUtils;

/**
 * Created by sherihan
 */
public class InfoFragmentDialog extends DialogFragment {

    Unbinder unbinder;

    @BindView(R.id.wViewArticleSource)
    WebView wViewArticleSource;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View dialogView = inflater.inflate(R.layout.info_dialog, container, false);
        unbinder = ButterKnife.bind(this, dialogView);

        String strContent = getArguments().getString("Content");
        if(strContent != null && !strContent.isEmpty())
        {
            wViewArticleSource.loadDataWithBaseURL("", strContent, "text/html", "utf-8", "");

        }

        return dialogView;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);


        return dialog;
    }

    public void onResume()
    {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);

    }

    @Override
    public void onStart() {
        super.onStart();

        if (getDialog() == null) {
            return;
        }

        // set the animations to use on showing and hiding the dialog
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimation);
    }

    @OnClick(R.id.imgClose)
    void closeDialog()
    {
        dismiss();
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();



    }




}
